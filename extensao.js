const minhasExtensoes = ["png", "gif"];

let imagemRecebida = "logoUFPR.jpg";

/* Armazene a extensão da imagemRecebida em uma variável, depois compare se essa extensão consta na variável
"minhasExtensoes". Se o formato for png ou gif imprima "recebido", se não for, imprima "formato inválido". 

Dica: existe um método (uma função) chamada split() que divide sua string em partes dependendo do caractere selecionado, 
caso queira usar esse método, basta digitar no fim da variável: .split('aquiVaiOCaractereParaDividirAString') 
existe um outro método bem útil chamado .pop(), tente usar ele após o .split() e veja o que acontece */

